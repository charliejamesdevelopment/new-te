
if(!cookieExists("authentication_token")) {
  swal({
    title: 'Oops!',
    type: 'error',
    html:
      "Your session has expired, please login!",
    confirmButtonText: 'Login',
    showCancelButton: false,
    allowOutsideClick: false
  }).then(function () {
    window.location = "/main/logout";
  }).catch(swal.noop);
}

$(document).ready(function() {
  getData(getUrl() + '/api/get_group_by_uuid/'+account_uuid, function(group) {
    if(group == 1) {
      $('.avatar-title').text("Something went wrong!");
    } else {
      $('.avatar-title').text(group.group_name);
    }
  });
});

$('#logout').click(function() {
  eraseCookie('authentication_token');
});

function redirectSwal(title, button_text, url) {
  swal({
    title: title,
    type: 'info',
    cancelButtonColor: '#d33',
    confirmButtonText: button_text,
    showCancelButton: false,
    allowOutsideClick: true
  }).then(function () {
    window.location = getUrl() + url;
  }).catch(swal.noop);
}

function reload() {
  location.reload();
}

function apiSwal(title, message, type, button_text, cancel_text, success_text, url) {
  swal({
    title: title,
    text: message,
    type: type,
    cancelButtonColor: '#d33',
    confirmButtonText: button_text,
    cancelButtonText: cancel_text,
    showCancelButton: false,
    allowOutsideClick: true
  }).then(function () {
    getData(location.origin + url, function(response) {
      if(response.response == 1) {
        swal({
          title: 'Error',
          text: response.message,
          type: 'error',
          confirmButtonText: 'Oops?',
          showCancelButton: false,
          allowOutsideClick: false
        }).catch(swal.noop);
      } else {
        var clicked = false;
        swal({
          title: 'Its done!',
          type: 'success',
          html: success_text,
          confirmButtonText: 'Okay',
          showCancelButton: false,
          allowOutsideClick: false
        }).then(function () {
          clicked = true;
          location.reload();
        }).catch(swal.noop);
        /*if(clicked != true) {
          setTimeout(reload, 5000);
        }*/
      }
    });

  }).catch(swal.noop);
}
