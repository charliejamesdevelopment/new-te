var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.post('/edit_appointment',function(req,res){
    if(req.body._id
    && req.body.time
    && req.body.date
    && req.body.status
    && req.body.type
    && req.body.additional_details) {
      utils.editAppointment(req.body, function(callback) {
        if(callback == 1) {
          res.send({"response" : 1, "message" : "Invalid id!"});
        } else {
          res.send({"response" : 0});
        }
      });
    } else {
      res.send({"response" : 1, "message" : "Invalid id!"});
    }
});

module.exports = router;
