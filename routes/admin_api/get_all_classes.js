var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');

router.get('/get_all_classes',function(req,res){
  utils.getAllClasses(function(callback) {
    if(callback == 1) {
      res.send({"response" : 1, "message" : "No classes are currently avaliable!"});
    } else {
      res.send(callback);
    }
  });
});

module.exports = router;
