var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_group_by_email/:email',function(req,res){
    utils.getGroupByEmail(req.params.email, function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Invalid email!"});
      } else {
        res.send(call);
      }
    });
});

module.exports = router;
