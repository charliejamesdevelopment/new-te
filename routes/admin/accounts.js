var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');

router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getUserData(req.session.uuid, function(obj) {
        if(obj == 1) {
          console.log("Couldn't find data for " + req.session.uuid);
        } else {
          utils.getGroupByUUID(req.session.uuid, function(g) {
            utils.getAllAccounts(function(accounts) {
              if(g.admin == false) {
                req.flash('error', 'Something went wrong...')
                res.redirect("/error");
              } else {
                res.render('admin/accounts', {
                  admin: g.admin,
                  user: obj,
                  group: g,
                  accounts: accounts,
                  active: "All Accounts",
                });
              }
            });
          });
        }
      });
    }
  });
});

module.exports = router;
