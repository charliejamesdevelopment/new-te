var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = new Schema({
  name: { type: String, required: true },
  classes: { type: Array, required: false },
  picture_url: { type: String, required: false, default: "" },
  dob: { type: String, required: true},
  created_at: Date,
  updated_at: Date
});

studentSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
studentSchema.set('collection', 'students');

var User = mongoose.model('Student', studentSchema);

// make this available to our users in our Node applications
module.exports = User;
