var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
router.get('/:id',function(req,res){
  if(req.session.uuid) {
    res.status(200)
    utils.getTicketData(req.params.id, function(ticket) {
      if(ticket == 1) {
        res.redirect("/");
      } else {
        utils.getChildrenByUUID(req.session.uuid, function(data) {
          utils.getUserData(req.session.uuid, function(obj) {
            utils.getGroupByUUID(req.session.uuid, function(g) {
              if(g.admin == true || ticket.author == req.session.uuid) {
                res.render('main/view_ticket', {
                  getStatus: appointment_utils.getStatus,
                  getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                  times: config.appointment.times,
                  types: config.appointment.types,
                  user: obj,
                  moment: moment,
                  group: g.name,
                  admin: g.admin,
                  data: data,
                  ticket: ticket,
                  active: "My Tickets"
                });
              } else {
                res.redirect("/");
              }
            });
          });
        });
      }
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
