var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
router.get('/:id',function(req,res){
  if(req.session.uuid) {
    res.status(200)
    utils.getUserData(req.params.id, function(user_data) {
      if(user_data == 1) {
        res.redirect("/");
      } else {
        utils.getChildrenByUUID(req.session.uuid, function(data) {
          utils.getUserData(req.session.uuid, function(obj) {
            utils.getGroupByUUID(req.session.uuid, function(g) {
              res.render('main/view_profile', {
                getStatus: appointment_utils.getStatus,
                getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                times: config.appointment.times,
                types: config.appointment.types,
                admin: g.admin,
                profile: user_data,
                user: obj,
                group: g,
                data: data,
                active: ""
              });
            });
          });
        });
      }
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
