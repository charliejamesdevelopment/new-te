var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
var year_utils = require('../utils/year');
router.get('/:id',function(req,res){
  if(req.session.uuid) {
    res.status(200)
    utils.getStudentData(req.params.id, function(user_data) {
      if(user_data == 1) {
        res.redirect("/");
      } else {
        utils.getChildrenByUUID(req.session.uuid, function(data) {
          utils.getUserData(req.session.uuid, function(obj) {
            utils.getStudentReports(user_data._id, function(report) {
              utils.getGroupByUUID(req.session.uuid, function(g) {
                res.render('main/view_student', {
                  year_utils: year_utils,
                  getStatus: appointment_utils.getStatus,
                  getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                  times: config.appointment.times,
                  types: config.appointment.types,
                  admin: g.admin,
                  profile: user_data,
                  reports: report,
                  user: obj,
                  group: g.name,
                  data: data,
                  active: "",
                  moment: moment
                });
              });
            });
          });
        });
      }
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
